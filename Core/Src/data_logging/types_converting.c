/**
  ******************************************************************************
  * @file    types_converting.c
  * @brief   Types Converting Module from integer to string "IntAsString_t".
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Oct 20, 2022
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "types_converting.h"

/* Private typedef -----------------------------------------------------------*/
/**
  * @brief  Degrees of ten
  */
typedef enum
{
	ONE_BILLION 			= 1000000000U,
	ONE_HANDRED_MILLION 	= 100000000U,
	TEN_MILLION  			= 10000000U,
	ONE_MILLION	  			= 1000000U,
	ONE_HANDRED_THOUSAND	= 100000U,
	TEN_THOUSAND			= 10000U,
	ONE_THOUSAND			= 1000U,
	ONE_HANDRED				= 100U,
	TEN						= 10U,
	ONE						= 1U
} TenToDegree_t;

/* Private define ------------------------------------------------------------*/
#define NOT_INITIALIZE_POSITION 10
#define LAST_POSITION 9

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static IntAsString_t    StringStructure;

/* Private function prototypes -----------------------------------------------*/
/* Private user code ---------------------------------------------------------*/
/* External variables --------------------------------------------------------*/
/* External functions --------------------------------------------------------*/

/**
  * @brief Converting of integer in string format
  * @param Number: integer value
  * @retval Structure of integer in string format
  */
IntAsString_t* convertIntToString(uint32_t Number)
{
	StringStructure.FirstNotNullPosition = NOT_INITIALIZE_POSITION;

	for (uint8_t i = 0; i < MAX_STRING_SIZE; i++)
	{
		StringStructure.String[i] = '0';
	}

	uint8_t tempCount = 0;

	void addCharIfNotNone(TenToDegree_t TenToDegree)
	{
		uint32_t tempNumber = Number;

		tempNumber /= TenToDegree;

		/* if there was already a meaningful number of the highest position */
		if (StringStructure.FirstNotNullPosition != NOT_INITIALIZE_POSITION)
		{
			tempNumber %= TEN;
		}

		if (tempNumber != 0)
		{
			StringStructure.String[tempCount] = '0' + (char)tempNumber;
			if (StringStructure.FirstNotNullPosition == NOT_INITIALIZE_POSITION)
			{
				StringStructure.FirstNotNullPosition = tempCount;
			}
		}

		tempCount++;
	}

	addCharIfNotNone(ONE_BILLION);
	addCharIfNotNone(ONE_HANDRED_MILLION);
	addCharIfNotNone(TEN_MILLION);
	addCharIfNotNone(ONE_MILLION);
	addCharIfNotNone(ONE_HANDRED_THOUSAND);
	addCharIfNotNone(TEN_THOUSAND);
	addCharIfNotNone(ONE_THOUSAND);
	addCharIfNotNone(ONE_HANDRED);
	addCharIfNotNone(TEN);
	addCharIfNotNone(ONE);

	if (StringStructure.FirstNotNullPosition == NOT_INITIALIZE_POSITION)
	{
		StringStructure.FirstNotNullPosition = LAST_POSITION;
	}

	return &StringStructure;
}










