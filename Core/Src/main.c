/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Oct 18, 2022
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "logging.h"
#include "dma.h"
#include "rcc.h"

/* Private variables ---------------------------------------------------------*/
static CommonBuffer_t TxRingBuffer;

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  initializeSystemClock();

  /* Initialize all configured peripherals */
  initializeGpioClock();
  initializeDma();
  UART_initialize();

  if (initializeCommonBuffer(RING_BUFFER_TX, &TxRingBuffer) != OK_STATUS)
  {
	  Error_Handler();
  }

  /* Infinite loop */
  while (1)
  {

	  static uint16_t RB_MaxNumberOfItems = 0;

	  if (addLogMessage(&TxRingBuffer, "System timer = ", HAL_GetTick()) != OK_STATUS)
	  HAL_Delay(1);

	  if (addLogMessage(&TxRingBuffer, "System timer = ", HAL_GetTick()) != OK_STATUS)
	  {
		  Error_Handler();
	  }
	  HAL_Delay(1);

	  uint16_t RB_tempNumberOfItems = RB_getNumberOfItems(TxRingBuffer.RingBuffer);
	  if (RB_tempNumberOfItems > RB_MaxNumberOfItems)
	  {
		  RB_MaxNumberOfItems = RB_tempNumberOfItems;
		  addLogMessage(&TxRingBuffer, "New maximum number of items = ", (uint32_t)RB_MaxNumberOfItems);
	  }

	  StatusFunctionExecution_t PrintStatus;
	  PrintStatus = print(&TxRingBuffer);
	  if (PrintStatus != OK_STATUS)
	  {
		  if (PrintStatus == NOT_FINISHED)
		  {
			  addLogMessageWithoutValue(&TxRingBuffer, "DMA is busy");
		  }
		  else
		  {
			  Error_Handler();
		  }
	  }
	  HAL_Delay(2);
  }
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
