/**
  ******************************************************************************
  * @file           : ring_buffer.h
  * @brief          : Header for ring_buffer.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  *
  * Author: dmitriy
  * Created on: Oct 18, 2022
  *
  ******************************************************************************
  */

#ifndef SRC_RING_BUFFER_H_
#define SRC_RING_BUFFER_H_

/* Includes ------------------------------------------------------------------*/
#include "common.h"

/* Exported types ------------------------------------------------------------*/
/**
  * @brief  Ring Buffer Status
  */
typedef enum
{
    RB_OK	 			= 0U,
    RB_INVALID_PARAMS 	= 1U,
    RB_ERROR 			= 2U,
	RB_FULL				= 3U,
	RB_EMPTY			= 4U
} StatusRingBuffer_t;

/**
  * @brief  Ring Buffer Structure
  */
typedef struct
{
  uint16_t Length;    		/*!< size of Ring Buffer */
  uint8_t* Head;    		/*!< pointer to array start */
  uint8_t* InputItem;    	/*!< pointer to input item */
  uint16_t NumberOfItems;   /*!< number of items */
} RingBuffer_t;

/* Exported constants --------------------------------------------------------*/
#define MAX_SIZE_DMA_BUFFER    65535U	  /*!< maximum size of DMA Buffer */

/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
StatusRingBuffer_t initializeRingBuffer(InterfaceBuffer_t);

RingBuffer_t* getRxRingBuffer(void);
RingBuffer_t* getTxRingBuffer(void);

StatusRingBuffer_t RB_putNewItems(RingBuffer_t*, const char*, uint8_t);
StatusRingBuffer_t RB_decreaseNumberOfItems(RingBuffer_t*, uint16_t);

uint16_t RB_getNumberOfItems(RingBuffer_t*);
uint8_t* RB_getOutputItemPointer(RingBuffer_t*);
uint16_t RB_getNumberOfItemsBeforeEnd(RingBuffer_t*);

/* Private defines -----------------------------------------------------------*/


#endif /* SRC_RING_BUFFER_H_ */
