/**
  ******************************************************************************
  * @file           : dma.h
  * @brief          : Header for dma.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Author: dmitriy
  * Created on: Oct 29, 2022
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef DMA_H_
#define DMA_H_

/* Includes ------------------------------------------------------------------*/
#include "common.h"
#include "stm32f4xx_hal.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
StatusFunctionExecution_t initializeDma(void);

/* Private defines -----------------------------------------------------------*/

#endif /* DMA_H_ */
